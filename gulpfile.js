const
    // development mode
    devBuild = true,

    // modules
    gulp = require('gulp'),
    noop = require('gulp-noop'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    htmlclean = require('gulp-htmlclean'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    concate = require('gulp-concat'),
    rename = require('gulp-rename'),
    sync = require('browser-sync').create(),

    // folders
    src = 'src/',
    build = 'public/';

// Images processing
function images() {
    const input = src + 'images/**/*';
    const output = build + 'assets/images/';

    return gulp.src(input)
        .pipe(newer(output))
        .pipe(imagemin())
        .pipe(gulp.dest(output));
}

// HTML processing
function html() {
    const input = src + 'html/**/*';
    const output = build;

    return gulp.src(input)
        .pipe(newer(output))
        .pipe(devBuild ? noop() : htmlclean())
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// CSS processing
function css() {
    const input = src + 'scss/main.scss';
    const output = build + 'assets/css/';

    return gulp.src(input)
        .pipe(newer(output))
        .pipe(devBuild ? sourcemaps.init() : noop())
        // .pipe(sass({
        //     outputStyle: 'compressed'
        // }).on('error', sass.logError))
        .pipe(sass({
            errLogToConsole: devBuild,
            outputStyle: devBuild ? 'expanded' : 'compressed'
        }).on('error', sass.logError))
        .pipe(concate('bundle.css'))
        .pipe(devBuild ? sourcemaps.write() : noop())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// JS processing
function js() {
    const input = src + 'js/**/*';
    const output = build + 'assets/js/';

    return gulp.src(input)
        .pipe(newer(output))
        .pipe(noop())
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// watch for file changes
function watch(done) {
    sync.init({
        server: {
            baseDir: './' + build
        }
    });

    sync.reload();

    
    // images changes
    gulp.watch(src + 'images/**/*', images);

    // html changes
    gulp.watch(src + 'html/**/*', html);

    // scss changes
    gulp.watch(src + 'scss/**/*', css);

    // js changes
    gulp.watch(src + 'js/**/*', js);


    done();
}


// Create single tasks
exports.html = gulp.series(images, html);
exports.css = css;
exports.js = js;
exports.watch = watch;


// Create all build tasks
exports.build = gulp.parallel(exports.html, exports.css, exports.js);


// Create default task
exports.default = gulp.series(exports.build, exports.watch);